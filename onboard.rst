============================
Getting People To Contribute
============================

Documentation provides a platform for your first contributions.
A lot of people have never contributed before, and documentation changes are a lot less scary than code changes.

- Make new contributers feel welcome
- Be a mentor
- Be helpful
- Communicate with new contributers, if they submit a PR, say thank you and give positive feedback
- Merge fast or explain why you can't merge.
  Nothing is more frustrating for them to keep waiting and nothing happens
- Provide easy and small tasks, they can work on -> fast positive results -> happy new contributor
- Keep them up to date about upcoming developments, plans and changes -> involve them
