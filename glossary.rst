==========
 Glossary
==========

.. glossary:: :sorted:

    Plone
        A powerful, flexible Content Management Solution.
        `Plone CMS Website <https://plone.org>`_

    VirtualBox
        A general-purpose full virtualizer for x86 hardware, targeted at server, desktop and embedded use.
        `VirtualBox Website <https://www.virtualbox.org/wiki/VirtualBox>`_

    DDD
        Documentation Driven Development

    RDD
        Readme Driven Development

    Python egg
        A widely used Python packaging format which consists of a zip or ``.tar.gz`` archive with some metadata information.
        It was introduced by `setuptools <https://pypi.python.org/pypi/setuptools>`_

        A way to package and distribute Python packages.
        Each egg contains a ``setup.py`` file with metadata (such as the author's name and email address and licensing information),
        as well as information about dependencies.
        ``setuptools``, the Python library that powers the egg mechanism,
        is able to automatically find and download dependencies for eggs that you install.
        It is even possible for two different eggs to concurrently use different versions of the same dependency.
        Eggs also support a feature called *entry points*, a kind of generic plug-in mechanism.

    GitHub
        A web-based Git repository hosting service.
         It offers all of the distributed version control and source code management (SCM) functionality of Git as well as adding its own features.

    HIT
        stands for Human-Interface-Test, real humans will read and test the documentation.

    Sphinx
        Sphinx is a tool that makes it easy to create intelligent and beautiful documentation.
        `Sphinx Website <http://www.sphinx-doc.org/en/stable/>`_

    Docker
        is a software containerization platform.
        `Docker Website <https://www.docker.com/>`_
