========
Exercise
========

Let's take an existing project, with many features but underwhelming documentation, and see if we can come up with a plan on how to improve it.

https://github.com/collective/eea.facetednavigation

Note that this is **no way** meant to be extra-critical of this particular project.
Quite the opposite, in fact.
This is a very nice project, with a good eye for usability and can bring real benefits to any website.

We picked this one because it is:

- Very useful
- Non-trivial, with many features
- Already has some good parts
- But could be much improved with relatively simple means

Ideally, we will come up with concrete pull requests to improve their documentation.
That will help the authors, the integrators picking the product, and the site authors using it.

So, by picking eea.facetednavigation as the training material, we hope we can achieve multiple goals.
Not only a real exercise, using a real project.
But also giving back to improve the project.
Open source as it should be!


Exercise 1:

- Identify some problems with the current documentation
- Make a list of them, try to assess how big each task would be
- Identify skillsets needed to improve the documentation

Exercise 2:

- Identify the intended audiences
- Design a new structure
- What would be an appropriate first page?
- Where would installation instructions go?
- Where would changelog go?
- Make a division in possible chapters

Exercise 3:

- Re-write the "what does it do" section ("readme")
- Identify where use-cases should go
- Identify where screencasts would be helpful
- Identify if they should be screencasts or animated gifs
- Practice in making simple screencast/gif

The outputs we hope to achieve:

- Make concrete suggestions for improvements
- Where possible, make them as pull requests
- Where not possible, see if we can at least formulate them as "improvement proposals" with necessary skills, so other people can pick them up
