=======
Editors
=======

ReText
======

Simple but powerful editor for Markdown and reStructuredText.

.. image:: ../_static/mr.otlet_retext.png
   :align: center
   :alt: Picture of a open ReText editor

`ReText Website <https://github.com/retext-project/retext>`_


Sublime Text 3
==============

`Sublime Text 3 <https://www.sublimetext.com/3>`_ is a sophisticated text editor for code, markup and prose.

.. image:: ../_static/mr.otlet_sublime-omi.gif
   :align: center
   :alt: Picture of a open sublime using a preview add-on

Add-ons
-------

Easy
~~~~

* reStructuredText-linter
* omni-preview
* word-count
* language-tool
* git-gutter


Advanced
~~~~~~~~

* write-good
* coala
* builder-plugins

