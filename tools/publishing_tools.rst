================
Publishing Tools
================

Read the Docs
-------------
Read the Docs hosts documentation, making it fully searchable and easy to find.

.. image:: ../_static/rtd-website.png
   :align: center
   :alt: Read the Docs Website

`Read the Docs Website <https://readthedocs.org/>`_

GitBook
-------
GitBook is a modern publishing toolchain. Making both writing and collaboration easy.

.. image:: ../_static/gitbook-website.png
   :align: center
   :alt: GitBook Website

`GitBook Website <https://www.gitbook.com/>`_

readme.io
---------
Developer and Documentation hub.

.. image:: ../_static/readmeio-website.png
   :align: center
   :alt: readme.io Website

`readme.io Website <https://readme.io/>`_

Doctor
------
Doctor is a documentation master for your docs

.. image:: ../_static/doctor.png
   :align: center
   :alt: Doctor App

GitHub Pages
------------

You can use GitHub Pages for hosting your documentation.

GitHub /docs feature
~~~~~~~~~~~~~~~~~~~~

**NEW** Publishing your GitHub Pages site from a /docs folder on your master branch.

Source:

.. image:: ../_static/example-repo.png
   :align: center
   :alt: Example Repo Source on GitHub

Output:

.. image:: ../_static/example-repo-build.png
   :align: center
   :alt: Picture of build docs of the example-repo

