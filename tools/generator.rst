==========
Generators
==========

MkDocs
-------
A fast, simple and downright gorgeous static site generator that's geared towards building project documentation.

.. image:: ../_static/mkdocs-website.png
   :align: center
   :alt: MkDocs Website

`MkDocs Website <http://www.mkdocs.org/>`_

Sphinx
------
Sphinx is a tool that makes it easy to create intelligent and beautiful documentation.

.. image:: ../_static/sphinx_website.png
   :align: center
   :alt: Sphinx Website

`Sphinx Website <http://www.sphinx-doc.org/en/stable/>`_


Asciidoctor
-----------
A fast text processor & publishing toolchain for converting AsciiDoc to HTML5, DocBook & more.

.. image:: ../_static/asciidoctor-website.png
   :align: center
   :alt: Asciidoctor Website

`Asciidoctor Website <http://asciidoctor.org/>`_

Doxygen
-------
Doxygen is the de facto standard tool for generating documentation from annotated C++ sources and other  popular programming languages.

.. image:: ../_static/doxygen-website.png
   :align: center
   :alt: Doxygen Website

`Doxygen Website <http://www.stack.nl/~dimitri/doxygen/>`_.

Jekyll
------
Transform your documentation into a static website.

.. image:: ../_static/jekyll-website.png
   :align: center
   :alt: Jekyll Website

`Jekyll Website <https://jekyllrb.com/>`_

Hugo
----
A fast and modern static website engine.

.. image:: ../_static/hugo-website.png
   :align: center
   :alt: Hugo Website

`Hugo Website <https://gohugo.io/>`_.

