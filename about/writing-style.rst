=============
Writing Style
=============

This section describes general writing style guidelines that
documentation contributors should follow to ensure consistency
throughout all technical publications.

.. toctree::
   :maxdepth: 2

   writing/general-writing-guidelines.rst
   writing/word-choice.rst
   writing/headings.rst
   writing/lists.rst

Parts of the guidelines are taken from `OpenStack <http://docs.openstack.org/contributor-guide/writing-style.html>`_
and from `Plone <http://docs.plone.org/about/contributing.html>`_.


