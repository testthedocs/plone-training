==========================
Building The Documentation
==========================

The documentation is based on [:term:`Sphinx`].

For building the HTML version of this training we use a[:term:`Docker`] based application called `ttd.docs-builder <https://github.com/testthedocs/ttd.docs-builder/>`_.

For more information on how to install ttd.docs-builder, please follow the `docs <https://github.com/testthedocs/ttd.docs-builder/blob/master/docs/install.rst>`_.

Usage
=====

.. code-block:: bash

    Usage: docsbuilder [option]
    --help, -h => show this help
    -html => builds our training docs
    -testbuild => runs a testbuild in nit-picky mode
