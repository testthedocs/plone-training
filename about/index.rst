========================
About This Documentation
========================

.. toctree::
   :hidden:
   :maxdepth: 2

   writing-style
   building
   thanks
