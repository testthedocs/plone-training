=============================
Documentation Basics Training
=============================

**Docs or it didn't happen**

Documentation can help you plan, write and debug your application.
Learn about different documentation setups and software.

Contribute
==========

- `Issue Tracker <https://github.com/testthedocs/ttd.training-docs-basics/issues>`_
- `Source Code <https://github.com/testthedocs/ttd.training-docs-basics>`_

Support
=======

If you are having issues, please let us know.

License
=======

The project is licensed under the `Creative Commons Attribution 4.0 International License <https://creativecommons.org/licenses/by/4.0/>`_.

