=======
Roundup
=======

- Conducting usability tests for documents
- Developing a content strategy that can help tech writers
- Using social media to increase visibility of tech content
- Collaborating with UI developers and designers
- Creating quick personas and journey maps
- Developing walkthroughs for onboarding and advanced users
- Work together with your support team
- You don’t need a screen shot for every step
- Screen-shots are most effective when they are meant to serve a specific purpose
- Judge a doc on how functional it is. Does it achieve its goals? If not, doesn't matter how well-written it is
- "80% correct is better than 80% complete". What a great way to emphasise content.
