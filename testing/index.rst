=======
Testing
=======

.. toctree::
   :hidden:
   :maxdepth: 2

   basic_testing
   look_and_feel

**The key to writing good documentation: Testing your instructions !**

* Writing good documentation requires you to set up a test environment and test all of your instructions -- testing the instructions yourself and against a user.
* Testing instructions can be time consuming and tricky, especially with developer documentation.
* It's hard to see past personal blind spots and assumptions.
* But testing instructions gives you access to insight that makes your documentation much more accurate and useful.
