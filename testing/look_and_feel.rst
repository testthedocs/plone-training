======
Visual
======

Checking The Look And Feel
==========================

There are different *levels* of visual testing.

* Is my syntax OK ?
* How does my docs looks like in HTML ?


Getting A Visual Impression Of Your Changes
===========================================

Easy
----

* Use a editor with rst preview

  * Sublime Text 3 with Omni-preview
  * Vim with rst-preview

* pandoc

  * Online version

Medium
------

* mr.docs


Advanced
--------

* Sphinx
* pandoc locally installed
