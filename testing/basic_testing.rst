==============
Basic Testing
==============

Human-Interface-Test
====================

The :term:`HIT` [Human-Interface-Test] is the **most** important test.

Let user test your documentation, choose user according to the addressed audience of the documentation and let them test.

* Do they understand your documentation ?
* Are they able to follow ?
* Too simple ?
* Too complicated ?
* Forgot something ?
* Do the docs work ?



Analytics
=========

* Make sure user find what they are looking for

Spell-Check
===========

* Directly in editor, as it often involves choosing an alternative
* As a linter in automated tests

Link-Check
==========

* If you work on a document, always make sure that all links are working, it's easier and faster to check one file, than the whole docs

Consistency
===========

* Always the same term, GitHub not github or Github.

Writing
=======

* Follow your style-guide
* Catch insensitive, inconsiderate writing
* Suggest shorter sentences
* Avoid common pitfalls in grammar and style
