=============================
Documentation Basics Training
=============================

**Docs or it didn't happen !**

.. toctree::
   :hidden:
   :maxdepth: 2

   why_docs_are_important
   philosophy
   strategy
   tone
   style-guide
   writing
   exercise_readme
   markup-format
   tools/index
   testing/index
   plone/overview
   plone/setup
   plone/how_to_contribute
   plone/contribute_via_feedback
   screenshots
   screencasts
   onboard
   roundup
   glossary
   about/index

.. image:: _static/write-drunk-edit-sober.png
   :alt: Picture 'Write drunk, edit sober'
