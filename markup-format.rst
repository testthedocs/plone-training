==============
Markup Format
==============

reStructuredText
----------------
reStructuredText is an easy-to-read, what-you-see-is-what-you-get plaintext markup syntax and parser system.
`reST Website <http://docutils.sourceforge.net/rst.html>`_

Markdown
--------
Markdown is a text-to-HTML conversion tool for web writers
`Markdown Website <https://daringfireball.net/projects/markdown/>`_

AsciiDoc
--------
AsciiDoc is a text document format for writing notes, documentation and more.
`AsciiDoc Website <http://www.methods.co.nz/asciidoc/>`_

DocBook
-------
A semantic markup language for technical documentation.

