=======================
Contribute Via Feedback
=======================

This way is the most suited one for everyone, you do not need to be a developer and you do not need a :term:`GitHub` account.

Feedback Form
==============

.. image:: ../_static/feedback.png
   :align: center
   :alt: Picture of feedback widget

**Click on 'Feedback'**

.. image:: ../_static/feedback1.png
   :align: center
   :alt: Open feedback form

**Fill in the form**

.. image:: ../_static/feedback2.png
   :align: center
   :alt: Picture of feedback form with example feedback

**Click on send**

