==========
Contribute
==========
How to get in contact and contribute.

Every contribution is equal important !

Get In Contact
===============

- Community Forum
- IRC
- Twitter
- Feedback button on docs.plone.org

Quickstart
==========

If you have already a GitHub account, please do not hesitate to open a ticket on `GitHub <https://github.com/plone/documentation/issues>`_ .

If you do not have one, please use the 'Feedback' widget on http://docs.plone.org.

.. image:: ../_static/feedback.png
   :align: center
   :alt: Picture of feedback widget
