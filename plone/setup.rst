=========================
Plone Documentation Setup
=========================

Structure
==========
All documentation is located on :term:`GitHub`.
Depending on the kind of docs, it is located in our main documentation repository or in the code repository of the :term:`Python egg`.

Overview
=========

.. image:: ../_static/docs_plone_org_dia.png
   :align: center
   :alt: Visual about the setup

Documentation
--------------
https://github.com/plone/documentation => main documentation

.. image:: ../_static/github_plone-docs-repo.png
   :align: center
   :alt: Plone Documentation Repository

Example of *fetching*

https://github.com/plone/ansible-playbook/tree/master/docs

.. image:: ../_static/plone-ansible.png
   :align: center
   :alt: Plone Ansible Repository

An Ansible playbook for automated deployment of full-stack Plone servers.

.. image:: ../_static/plone-ansible-docs.png
   :align: center
   :alt: Plone Ansible Docs in Repository


Build Tool
----------
Collects, tests and builds the documentation

.. image:: ../_static/papyrus.png
   :align: center
   :alt: Plone Papyrus Repository

https://github.com/plone/papyrus

.. image:: ../_static/papyrus-assembly.png
   :align: center
   :alt: Plone Papyrus Assembly Script




