===========================
Plone Documentation Project
===========================

Overview
=========

- Source: On GitHub
- Markup Format: reStructuredText
- build with Sphinx
- available as HTML on docs.plone.org

Why We Choose For reST and Sphinx
==================================
- Sphinx is written in Python, that makes it easier for us [Plone Community] to extend functionality.
- Sphinx and reST were designed for writing documentation and have the advantage of consistency.
- reStructuredText provides standard extension mechanisms called directives and roles which make all the difference.

Role Example
-------------
Code
~~~~
.. code-block:: rst

   .. example-code::
        .. code-block:: Debian

            sudo apt install htop

        .. code-block:: Fedora

            sudo dnf install htop

Output
~~~~~~
.. image:: ../_static/example.gif
   :align: center
   :alt: Example how it looks like as generated HTML

extensibility, semantic rather than presentational markup, support for multiple languages (*), standard API.

