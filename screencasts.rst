===========
Screencasts
===========

Screencasts (video or animated images) can be a powerful means to document your software.

They can serve a dual purpose as well, to both serve as "demonstration" and as actual documentation.

However, there are also drawbacks.

Long-form video
===============

In this setting, long-form can be anything from 2 minutes to half an hour or more.
These kind of videos are best used as 'demonstration'-type, which means you will want to spend some time preparing them.

- Create a script with both what you want to show (visual) and what you want to tell (audio)
- make sure your setup is working: a good microphone/headset, post-processing software
- do test runs, until you can go through the script fluidly
- speak slow, be deliberate and articulate
- use a neutral voice and 'international English', avoid country-specific idiom as far as possible
- make sure your mouse pointer is visible. Many operating systems have built-in ways to make it bigger and/or have a 'glow'

Long-form video will almost always need post-processing.
It needs an introduction screen, and usually closing credits with further links.

Do remember that 85% of videos these days are viewed **without sound**!!
That means you will want to have captions or text-overlays with the text you are speaking.
(captions are better for accessibility, text-overlays can look nicer)

Very useful is Youtube's "auto-caption" facility: you can upload a video, and have Youtube do a rough automated voice-recognition and captioning.
You will most likely need to improve, but it is a huge time-saver.


Overall, long-form videos require a considerable time-investment, and are very hard to keep up to date.

As pure documentation, that is probably not worth it. As combined marketing and documentation, it can be an option.


Animated Screenshots and Short Videos
=====================================

These are a much more viable option.

Some can be scripted, and the output can be in the form of an animated gif.
Animated gifs have the big advantage they can be stored directly in your docs; videos are usually outsourced on Youtube or Vimeo, which leads to more distraction.

General recommendation: gifs are best for short segments, more like a few seconds.
Think 'rearrange content', or 'fill in a form with some dropdown options'.
Especially concepts like 'drag and drop', or 'pick up a corner of a widget to make it larger or smaller' are better explained in a visual way.

As animated gifs are just images, you can use proper accessibility methods on them: hide when they make no sense for screenreader-users, use an `alt` tag to describe them, etcetera.
This is an advantage over using video-services like YouTube/Vimeo.

Creating them requires special tools.

For Windows, `screentogif <http://screentogif.codeplex.com/>`_ is open-source and useful.

For OS X, there is `LICEcap <http://www.cockos.com/licecap/>`_, which is free but a bit older.
There is also `GiFox <http://gifox.io>`_ but that is non-free in all meanings.

On Linux, there is a command-line only utility called "byzanz".
Most distributions have it in their standard repositories.
It is slightly complicated to use, as you will have to set the x- and y-coordinates of the part you want to record.
The advantage is, that it can be scripted, and thus in theory can also work with browser automation technologies.

There is also a new and simple tool, `peek <https://github.com/phw/peek/releases>`_ but at the moment there are only binary installers available for Ubuntu, not other flavours.
There is active development on it, so it may be worth checking out if it works on your setup. *Using* it is simpler than the alternatives.

